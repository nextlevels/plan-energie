<?php
/**
 * Grundeinstellungen für WordPress
 *
 * Zu diesen Einstellungen gehören:
 *
 * * MySQL-Zugangsdaten,
 * * Tabellenpräfix,
 * * Sicherheitsschlüssel
 * * und ABSPATH.
 *
 * Mehr Informationen zur wp-config.php gibt es auf der
 * {@link https://codex.wordpress.org/Editing_wp-config.php wp-config.php editieren}
 * Seite im Codex. Die Zugangsdaten für die MySQL-Datenbank
 * bekommst du von deinem Webhoster.
 *
 * Diese Datei wird zur Erstellung der wp-config.php verwendet.
 * Du musst aber dafür nicht das Installationsskript verwenden.
 * Stattdessen kannst du auch diese Datei als wp-config.php mit
 * deinen Zugangsdaten für die Datenbank abspeichern.
 *
 * @package WordPress
 */

// ** MySQL-Einstellungen ** //
/**   Diese Zugangsdaten bekommst du von deinem Webhoster. **/

/**
 * Ersetze datenbankname_hier_einfuegen
 * mit dem Namen der Datenbank, die du verwenden möchtest.
 */
define('DB_NAME', 'usr_p352890_1');

/**
 * Ersetze benutzername_hier_einfuegen
 * mit deinem MySQL-Datenbank-Benutzernamen.
 */
define('DB_USER', 'p352890');

/**
 * Ersetze passwort_hier_einfuegen mit deinem MySQL-Passwort.
 */
define('DB_PASSWORD', 'ovufoDul-695');

/**
 * Ersetze localhost mit der MySQL-Serveradresse.
 */
define('DB_HOST', 'db1233.mydbserver.com');

/**
 * Der Datenbankzeichensatz, der beim Erstellen der
 * Datenbanktabellen verwendet werden soll
 */
define('DB_CHARSET', 'utf8');

/**
 * Der Collate-Type sollte nicht geändert werden.
 */
define('DB_COLLATE', 'utf8_general_ci');

/**#@+
 * Sicherheitsschlüssel
 *
 * Ändere jeden untenstehenden Platzhaltertext in eine beliebige,
 * möglichst einmalig genutzte Zeichenkette.
 * Auf der Seite {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * kannst du dir alle Schlüssel generieren lassen.
 * Du kannst die Schlüssel jederzeit wieder ändern, alle angemeldeten
 * Benutzer müssen sich danach erneut anmelden.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'HRSSgH,D;1y-twY4y37:n)WuU!<hYP=[5/{l&fcS=ae%ix-Csd%9t($=H@H$.eBB');
define('SECURE_AUTH_KEY',  'i(cqN1E^GG+#;-jI|as+u-I6OV4%V1;EPI[oxWL/fWvK(@gST!c1$)c^.}<xM$%B');
define('LOGGED_IN_KEY',    'PV(fy DR1KbJZ w(-Dq)iSS@s%ERcuz-ad$zXvOL]CD,m[q?s}s-o{8TA4XEW}_`');
define('NONCE_KEY',        'y[N`GW?hqkN6Rrctsg%e>b^1%br,y+$TnNed4W&BS$#u?S.{qlo~<q^[8PblWH`y');
define('AUTH_SALT',        '(R+kODaQ?=<|BkHH@@+Kon_,U-;Z&v~2nz-3C9pyJ6-2?Y?6~|0GW(*is7veD#ip');
define('SECURE_AUTH_SALT', '-%V~IL@/!YsVuEBiT(;Q3n)Fh%xP*<[yEM_.__e-S)jxvibhh9c}LO$:S4[|g6y$');
define('LOGGED_IN_SALT',   ')v/VM ,]a]s7rO}+nt!Ui]Psu3>3_HT0.4:uH+{%=fIR;tcdFYZ5fyKPh9? vPQc');
define('NONCE_SALT',       'N 0OGh-v]e1b(`|D[JO[W_ic)_=bR6m3G dr,%5|#U<2C`_}+MSJUA.NO<#r=nAU');

/**#@-*/

/**
 * WordPress Datenbanktabellen-Präfix
 *
 * Wenn du verschiedene Präfixe benutzt, kannst du innerhalb einer Datenbank
 * verschiedene WordPress-Installationen betreiben.
 * Bitte verwende nur Zahlen, Buchstaben und Unterstriche!
 */
$table_prefix  = 'wp_';

/**
 * Für Entwickler: Der WordPress-Debug-Modus.
 *
 * Setze den Wert auf „true“, um bei der Entwicklung Warnungen und Fehler-Meldungen angezeigt zu bekommen.
 * Plugin- und Theme-Entwicklern wird nachdrücklich empfohlen, WP_DEBUG
 * in ihrer Entwicklungsumgebung zu verwenden.
 *
 * Besuche den Codex, um mehr Informationen über andere Konstanten zu finden,
 * die zum Debuggen genutzt werden können.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Das war’s, Schluss mit dem Bearbeiten! Viel Spaß beim Bloggen. */
/* That's all, stop editing! Happy blogging. */

/** Der absolute Pfad zum WordPress-Verzeichnis. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Definiert WordPress-Variablen und fügt Dateien ein.  */
define('CONCATENATE_SCRIPTS', false); 
require_once(ABSPATH . 'wp-settings.php');
