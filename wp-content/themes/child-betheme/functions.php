<?php

function child_betheme_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri(). '/style.css');
    wp_enqueue_style( 'parent-style-layout', get_template_directory_uri(). '/css/layout.css');

    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'parent-style' ));
    wp_enqueue_style( 'child-style-layout', get_stylesheet_directory_uri() . '/css/layout.css', array( 'parent-style-layout' ));
}
add_action( 'wp_enqueue_scripts', 'child_betheme_styles' );

function print_menu_shortcode($atts, $content = null) {
    extract(shortcode_atts(array( 'name' => null, ), $atts));
    return wp_nav_menu( array( 'menu' => $name, 'echo' => false ) );
}
add_shortcode('menu', 'print_menu_shortcode');

